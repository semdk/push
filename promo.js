// Показы внутренней рекламы

<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'promoView': {
      'promotions': [
      { 
        'id': 'PromoID1', // id баннера в админке https://i.imgur.com/KdmRAw5.png
        'name': 'Promo 1', // название баннера, есть в админке, есть в альте
        'position': 'slot1' // расположение баннера в списке галереи баннеров
      },
      {
        'id': 'PromoID2',
        'name': 'Promo 2',
        'position': 'slot2'
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Promotion Impressions',
  'gtm-ee-event-non-interaction': 'True',
});
</script>

// Клики по внутренней рекламе

<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'promoClick': {
      'promotions': [{
        'id': 'PromoID1',
        'name': 'Promo 1',
        'position': 'slot1'
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Promotion Clicks',
  'gtm-ee-event-non-interaction': 'False',
});
</script>
