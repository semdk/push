// Шаг 1. Переход в корзину (https://www.iport.ru/cart/). 

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'checkout': {
      'actionField': {'step': 1},
      'products': [{
        'name': 'Product 1', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // 1с артикул, вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 2 //         подхватываем соответствующее значение в товарной/сервисной области
      },
      {
        'name': 'Product 2', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 1 //         подхватываем соответствующее значение в товарной/сервисной области
      }
      ]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Checkout Step 1',
  'gtm-ee-event-non-interaction': 'False',
});


// Шаг 2. Переход на страницу оформление заказа (https://www.iport.ru/order/)

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'checkout': {
      'actionField': {'step': 2},
      'products': [{
        'name': 'Product 1', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 2 //         подхватываем соответствующее значение в товарной/сервисной области
      },
      {
        'name': 'Product 2', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 1 //         подхватываем соответствующее значение в товарной/сервисной области
      }
      ]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Checkout Step 2',
  'gtm-ee-event-non-interaction': 'False',
});


// Шаг 3. Заполнение контактных данных.

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'checkout': {
      'actionField': {'step': 3, 'option': 'Введите корректный телефон'}, /* передаем после заполнения контактных данных. Если все изначально введено корректно, 
                                                                          то передаем 'actionField': {'step': 3}
                                                                          В случае уведомления об ошибки в поле option передаем тип ошибки */    
      'products': [{
        'name': 'Product 1', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 2 //         подхватываем соответствующее значение в товарной/сервисной области
      },
      {
        'name': 'Product 2', // берем для каждого уже добавленого в корзину товара/услуги 
        'id': 'ID1', // вытягиваем из ссылок на товары или data-атрибутов
        'price': '23.5', // подхватываем соответствующее значение в товарной/сервисной области
        'category': 'Category 1/Subcategory 11', // берем из названия ?
        'quantity': 1 //         подхватываем соответствующее значение в товарной/сервисной области
      }
      ]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Checkout Step 3',
  'gtm-ee-event-non-interaction': 'False',
});

// Шаг 4. Выбор способа доставки.

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'checkout': {
      'actionField': {'step': 4, 'option': 'Новая почта'}, 
      // в option передаем итоговое значение выбранного способа доставки. Пока представлен только самовывоз в пуш передается название магазина после клика по области https://i.imgur.com/M7dh4iu.png
      'products': [{         // массив со всеми выбранными товарами и услугами
        'name': 'Product 1',
        'id': 'ID1',
        'price': '23.5',
        'brand': 'Brand 1',
        'category': 'Category 1/Subcategory 11',
        'variant': 'Variant 1',
        'quantity': 2
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Checkout Step 4',
  'gtm-ee-event-non-interaction': 'False',
});



// Шаг 5. Выбор способа оплаты  

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'checkout': {
      'actionField': {'step': 5, 'option': 'Онлайн-оплата'}, // в option передаем итоговое значение выбранного способа оплаты
      'products': [{
        'name': 'Product 1',
        'id': 'ID1',
        'price': '23.5',
        'brand': 'Brand 1',
        'category': 'Category 1/Subcategory 11',
        'variant': 'Variant 1',
        'quantity': 2
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Checkout Step 5',
  'gtm-ee-event-non-interaction': 'False',
});

