<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB', // всегда это значение
    'detail': {
      'actionField': {'list': 'Product Card'},
      'products': [{
        'name': 'Product 1', // название товара берем из h1
        'id': 'ID1', // sku тянем отсюда <span class="ProductTopstyles__ProductTop__codeNumber-z5ii7c-6 grPIUE"> или выделяем из адреса страницы - products/productID {id} /
        'price': '23.5', // тянем отсюда <span class="ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 dIDAUy">, очистив предварительно от &nbsp;
        'brand': 'Brand 1', // тянем из значения для свойства "производитель" https://i.imgur.com/HXexwaW.png 
     
        'category': 'Category 1/Subcategory 11',
        /*
        значение для category берем из хк, где удаляем первое и последнее значение
        например, для КТ https://stage.ssr.iport.ru/products/productID106147/ значение будет "iPhone/iPhone 13 mini"
        */
      }]
    },
   
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Product Details',
  'gtm-ee-event-non-interaction': 'True',
});
</script>