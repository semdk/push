<script>
// Measures product impressions and also tracks a standard
// pageview for the tag configuration.
// Product impressions are sent by pushing an impressions object
// containing one or more impressionFieldObjects.
dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',                       // Local currency is optional.
    'impressions': [
     {
       'name': 'Triblend Android T-Shirt',       // Name or ID is required.
       'id': '12345',
       'price': '15.25',
       'brand': 'Google',
       'category': 'Apparel',
       'list': 'Search Results', 
          /*list (строковая необязательная переменная) — название списка товаров, где находится товар, по которому пользователь кликнул. .
      Значение, которое будет передаваться вместо 'List 1' будет зависеть от того где совершен клик:
          если в листинге = 'Catalog Grid';
          если из блока акций на главной = 'Sale Timers';
          если из акции = 'Sales';
          если из ссылки на другие цвета из КТ = 'Product Colors';
          если ссылка из сравнения = 'Compare Row';
          если клик на ссылку из основных товаров в корзине = 'Basket Grid';
          если клик на ссылку из товаров аксы в корзине = 'Basket Accesories';
          если клик на ссылку из поисковых подсказок = 'Search Clue';
          если клик на ссылку с страницы результатов поиска = 'Search Results';
          Если товар не принадлежит какому-то из списков, следует отправлять пустую строку (в таком случае в отчетах вместо названия списка будет стоять not set)
      */
       'position': 1
     },
     {
       'name': 'Donut Friday Scented T-Shirt',
       'id': '67890',
       'price': '33.75',
       'brand': 'Google',
       'category': 'Apparel',
       'list': 'Search Results',
       'position': 2
     }]
  }
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Product Impressions',
  'gtm-ee-event-non-interaction': 'True',
});
</script>