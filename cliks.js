/* пуш который должен срабатывать при клике на товар в следующих областях
https://i.imgur.com/2yg0zf7.png	сетка товаров
https://i.imgur.com/N4gyD4b.png	акции
https://i.imgur.com/dodof1u.png	мультикарточка
https://i.imgur.com/b7H8TOE.png	блок "рекомендуем"
https://i.imgur.com/d9fJTA5.png	ссылки на товар и аксу
https://i.imgur.com/nuhlcXr.png	ряд цветов

*/


<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB', // всегда это значение
    'click': {
      'actionField': {'list': 'List 1'}, 
      /*list (строковая необязательная переменная) — название списка товаров, где находится товар, по которому пользователь кликнул. .
      Значение, которое будет передаваться вместо 'List 1' будет зависеть от того где совершен клик:
          если в листинге = 'Catalog Grid';
          если из блока акций на главной = 'Sale Timers';
          если из акции = 'Sales';
          если из ссылки на другие цвета из КТ = 'Product Colors';
          если ссылка из сравнения = 'Compare Row';
          если клик на ссылку из основных товаров в корзине = 'Basket Grid';
          если клик на ссылку из товаров аксы в корзине = 'Basket Accesories';
          если клик на ссылку из поисковых подсказок = 'Search Clue';
          если клик на ссылку с страницы результатов поиска = 'Search Results';
          Если товар не принадлежит какому-то из списков, следует отправлять пустую строку (в таком случае в отчетах вместо названия списка будет стоять not set)
      */
       'products': [{
           'name': 'Product 1', // название товара
       /*
       В листинге, из блока акций  извлекаем из <h3 class="CatalogItemstyles__CatalogItem__Title-sc-8mov5i-5 fmpZbP">
       в корзине если это основной товар извелкаем из <a class="BasketItemTablestyles__BasketItemTable__Title-sc-1lya7d7-9 bfYELm">
       
       в карточке товаров альтернативные варианты необходимо дополнить атрибутом data-name куда передавать название альтернативной версии товара и оттуда уже извлекать в name
       аналогично требуется дополнить атрибутом data-name ссылки на товары в сравнении и тянуть оттуда
      
       */
           'id': 'ID1', // извлекаем из самой ссылки products/productID {id} /
           'price': '23.5', // Берем из тега с классом ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 dIDAUy там где цена представлена
           'category': 'Category 1/Subcategory 11',
       /*
       если клик происходит в листинге (т.е. путь содержит /catalog/), то сюда передаем все хлебные крошки за исключением первого пункта
       например, для страницы https://stage.ssr.iport.ru/catalog/kabeli_i_perekhodniki/ здесь будет значение "Аксессуары/Питание и кабели"
       
       если клик происходит в карточке товара, то первое и последнее значение хк удаляем. 
       например, для этой КТ должно уйти https://stage.ssr.iport.ru/products/productID106147/  "iPhone/iPhone 13 mini"
       
       если клик происходит на статичной или иной странице не относящейся к каталогу, то передаем все хлебные крошки за исключением первого пункта
       
       */
           'position': 1
       /*
       position (всегда — целое число) — положение товара в данном списке. Левая верхняя позиция — это номер один.
       */
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Product Clicks',
  'gtm-ee-event-non-interaction': 'False',
});
</script>