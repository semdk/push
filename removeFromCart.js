/*

первая развилка - попап или страница корзины
вторая развилка - массовое удаление (очистить корзину) или поштучное
  массовое удаление - может быть "очистить корзину", может быть прожатием крестика у товара с количеством > 1
третья развилка - товар или услуга

*/

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'remove': {
      'products': [{
        'name': 'Product 1', /*

          * при удаление в попапе:
             * имя удаляемого товара тянется из <h6 class="BasketPopupstyles__BasketPopupItem__Title-sc-4lnp3e-9 bDHjGK">
             * если клик был на "очистить корзину" - пуш может быть один, но для каждого товара, включая услуги, собирается полный массив данных в {'products': [{name1, id1, etc1}, {name2, id2, etc2}]}
          
          * при удалении в корзине:
            * имя товара берем из поля <a href="" class="BasketItemTablestyles__BasketItemTable__Title-sc-1lya7d7-9 bfYELm">
              * если у товара были выбраны услуги, то вместе с удаление товара, пуш должен отдельной записью включить в себя удаление услуги
            * если удаляется только услуга, то имя берем из поля <h4 class="ServiceRowstyles__ServiceRow__Title-urtfmv-1 cvIXfw">
            * если это массовое удаление, то для каждого товара и для каждой активной услуги собирается полный массив данных в {'products': [{name1, id1, etc1}, {name2, id2, etc2}]}

        */

        'id': 'ID1',        /*
          
          * при удаление в попапе извлекаем из ссылок на товар или услугу - products/productID {id} /
            * если удаление массовое, то извлекаем для каждого товара
          
          * при удалении в корзине:
            * если удаление штучное:
              * если товар, то извлекаем из ссылки - products/productID {id} /
              * если услуга, то тут надо либо добавить data-id в нужный тег, либо добавить ссылки в верстку
            * если удаление массовое:
              * для всех выбранных товаров и активных услуг извлекаем id
        */

        'price': '23.5',

        /*
        
        при удаление в попапе цену для товара берем из <span class="ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 dIDAUy">
          * если удаление массовое, то вытягиваем значения для всех товаров 

        при удалении в корзине:
          * если товар или услуга, то берем отсюда <span class="ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 dIDAUy">

        */

        'category': 'Category 1/Subcategory 11',
      /*
        если удаление происходит в карточке товара, то передаем "Product Card"
        для всех остальных страниц передаем все хлебные крошки за исключением первого пункта

      */


        'quantity': 1

        /*
        при массовом удалении передаем для каждой продуктовой записи количество удаляемого товара

        при удалении в попапе ищем внутри товарного блока <span class="Counterstyles__Counter-sc-3o1eab-0 dRUxMn BasketPopupstyles__BasketPopupItem__Counter-sc-4lnp3e-7 bunTOE">:
          * если такого блока нет, то передаем 1
          * если есть, то вложенное внутрь него значение

        при удалении в корзине: 

      */
      }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
  'gtm-ee-event-non-interaction': 'False',
});
