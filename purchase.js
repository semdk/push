<script>
// Send transaction data with a pageview if available
// when the page loads. Otherwise, use an event when the transaction
// data becomes available.
dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
dataLayer.push({
  'ecommerce': {
    'purchase': {
      'actionField': {
        'id': 'T12345',                         // ID заказа
        'affiliation': 'Online Store',          // Магазин, который выбран для самовывоза, или курьерская доставка
        'revenue': '35.43',                     // общая сумма заказа
        'shipping': '5.99',                     // стоимость доставки
        'coupon': 'SUMMER_SALE'                 // использованный промокод
      },
      'products': [{                            // список купленных товаров
        'name': 'Triblend Android T-Shirt',     // Название товара
        'id': '12345',                          // номер sku
        'price': '15.25',                       // Цена товара
        'brand': 'Google',                      // не будет такого
        'category': 'Apparel',                  // хзхз
        'variant': 'Gray',                      // хзхз
        'quantity': 1,                          // количество купленного товаро
       },
       {
        'name': 'Donut Friday Scented T-Shirt',
        'id': '67890',
        'price': '33.75',
        'brand': 'Google',
        'category': 'Apparel',
        'variant': 'Black',
        'quantity': 1
       }]
    }
  }
});
</script>


// динамический ремаркетинг

<script>
   window.dataLayer = window.dataLayer || [];
   dataLayer.push({
       "transactionId": "sk1006",   // внутренний номер заказа (не более 100 символов)
       "transactionChannel": "adm", // параметр дедупликации (по умолчанию для Admitad Affiliate)
       "transactionAction": "1",    // код целевого действия (определяется при интеграции)
       "transactionProducts": [{
           "sku": "1354471145550",  // внутренний код продукта (не более 100 символов)
           "tariff": "2",           // код тарифа (определяется при интеграции)
           "price": "300.00",       // цена товара
           "priceCurrency": "RUB",  // код валюты ISO-4217 alfa-3
           "quantity": "2"          // количество товара
       }, {
           "sku": "1354471047246",
           "tariff": "2",
           "price": "500.00",
           "priceCurrency": "RUB",
           "quantity": "1"
       }]
   });
</script>