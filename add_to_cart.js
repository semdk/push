window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'RUB',
    'add': {
      'products': [{
        'name': 'Product 1', /* источник названия товара зависит от области, в которой должен сработать пуш
        
        для листинга и блока рекомендаций на статике это будет <h3 class="CatalogItemstyles__CatalogItem__Title-sc-8mov5i-5 fmpZbP">
        
        для основного товара в карточке товара - h1
        
        для аксессуаров в корзине - <h3 class="CatalogItemstyles__CatalogItem__Title-sc-8mov5i-5 fmpZbP">
        
        для услуг в карточке товара и в корзине - <h4 class="ServiceRowstyles__ServiceRow__Title-urtfmv-1 cvIXfw">
        
        для результатов поиска - 
        
        */ 
        
        'id': 'ID1', 
        
        /* для обычных товаров извлекаем из ссылки на товар products/productID {id} /
        
        для услуг потребуется добавить либо ссылку на услугу, либо атрибут data-id внутрь тега
        
        */
        
        'price': '23.5', 
        
        /* во всех случаях предварительно чистим от неразрывных пробелов и знака рубля, источник цены зависит от страницы активации пуша
        
        при добавлении товара из листинга или стандартного товарного блока для размещения на статике цену берем из <span class="ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 dIDAUy"> 
        
        при добавлении основного товара из карточки товара цену берем из поля <span class="ProductPricestyles__ProductPrice__MainPrice-sc-1ttsy8o-2 kfBFHl">
        
        при добавлении услуги в кт или в корзине цену берем из поля <div class="ProductPricestyles__ProductPrice-sc-1ttsy8o-0 ecahMM">
        
        при добавлении аксессуара в корзине берем из - <span class="CatalogItem_Ministyles__CatalogItemMini__Price-xtwkyw-1 ifMKwq">
        
        */

        'category': 'Category 1/Subcategory 11',
       
       /*
       для товаров: 
       
       * если добавление происходит в листинге (т.е. путь содержит /catalog/), то сюда передаем все хлебные крошки за исключением первого пункта
       например, для страницы https://stage.ssr.iport.ru/catalog/kabeli_i_perekhodniki/ здесь будет значение "Аксессуары/Питание и кабели"
       
       * если клик происходит в карточке товара, то передаем "Product Card"
       
       * если клик происходит на статичной или иной странице не относящейся к каталогу, то передаем все хлебные крошки за исключением первого пункта
       
       для услуг:
       
       * если добавление происходит из карточки товара, то передаем "Product Card"
       
       * если добавление происходит из корзины, то передаем хлебные крошки за исключением первого пункта
       
       */
         'quantity': 1
         
         /* 
        для товаров увеличиваем на 1 с каждым прожатием в кт или корзине <button class="CountFieldstyles__CountField__Button-sc-1g9ahei-3 ZaQtR js-focus-visible" type="button" aria-label="Увеличить количество товара на 1">
        
        для услуг увеличиваем на 1 с каждым прожатием в корзине <button class="CountFieldstyles__CountField__Button-sc-1g9ahei-3 ZaQtR js-focus-visible" type="button" aria-label="Увеличить количество товара на 1">
        
        надо подумать - отправлять ли с каждым кликом или сделать тайм-аут перед отправкой
         */
       }]
    }
  },
  'event': 'gtm-ee-event',
  'gtm-ee-event-category': 'Enhanced Ecommerce',
  'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
  'gtm-ee-event-non-interaction': 'False',
});